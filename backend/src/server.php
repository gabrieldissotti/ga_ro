<?php

/**
 * @OA\Info(title="GC Challenge", version="0.1")
 *
 * @OA\OpenApi(
 *     security={
 *         {"api_key": {}}
 *     }
 * )
 */

/**
 * @OA\SecurityScheme(
 *   securityScheme="api_key",
 *   type="apiKey",
 *   in="header",
 *   name="Authorization"
 * )
 */

namespace App;

require __DIR__.'/../vendor/autoload.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$app = new \Slim\App();

require 'database/index.php';
require __DIR__.'/app/middlewares/_index.php';
require 'routes.php';

class App
{
    public function __construct($router)
    {
        try {
            $this->database = new Database();

            $this->router = $router;

            $this->router->run();
        } catch (\Exception $th) {
            $this->errorHandler($th);
        }
    }

    public function errorHandler($th)
    {
        http_response_code(500);

        return print_r(json_encode([
            'error' => $th->getMessage(),
        ]));
    }
}

new App($app);
