<?php

use App\Controller;
use App\Middleware;

$app->add(Middleware\Auth::apply());

$app->group('/api', function (Slim\App $app) {
    $app->get('/events', Controller\Events::class.':index');
    $app->get('/events/{id}', Controller\Events::class.':show');
    $app->get('/events/{id}/groups', Controller\EventGroups::class.':index');

    $app->get('/matches', Controller\Matches::class.':index');

    $app->get('/ranking', Controller\Ranking::class.':index');
});

$app->get('/swagger-yaml', App\Lib\Swagger::class.':index');
