<?php

namespace App\Lib;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Swagger
{
    public function index(Request $req, Response $res, $args)
    {
        try {
            $openapi = \OpenApi\scan(__DIR__.'/../');

            return $res
                ->withAddedHeader('content-type', 'text/yaml')
                ->getBody()
                ->write($openapi->toYaml());
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }
}
