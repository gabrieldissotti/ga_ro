<?php

namespace App\Middleware;

use App\Model;

class Auth
{
    public static function apply()
    {
        return function ($request, $response, $next) {
            if ($request->getMethod() == 'OPTIONS') {
                return $response->withStatus(200);
            }

            if ($request->getUri()->getPath() == '/swagger-yaml') {
                return  $next($request, $response);
            }

            $authorization = $request->getHeader('Authorization');

            if (empty($authorization)) {
                return $response
                ->withStatus(400)
                ->withJson(['message' => 'Token not provided']);
            }

            $token = str_replace('Bearer ', '', $authorization[0]);

            $isValidToken = !empty(json_decode(Model\ApiToken::where('encryption_key', $token)->get()));

            if ($isValidToken) {
                return  $next($request, $response);
            }

            return $response
              ->withStatus(401)
              ->withJson(['message' => 'Inválid token']);
        };
    }
}
