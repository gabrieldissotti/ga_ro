<?php

namespace App\Model;

class Event extends AppModel
{
    public function groups()
    {
        return $this->hasMany('App\Model\Group');
    }
}
