<?php

namespace App\Model;

class ApiToken extends AppModel
{
    public function concurrent_teams()
    {
        return $this->hasMany('App\Model\ConcurrentTeam', 'team_id');
    }
}
