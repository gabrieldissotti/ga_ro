<?php

namespace App\Model;

class ConcurrentTeam extends AppModel
{
    public function group()
    {
        return $this->belongsTo('App\Model\Group', 'group_id');
    }

    public function team()
    {
        return $this->belongsTo('App\Model\Team', 'team_id');
    }

    public function matches_has_concurrent_teams()
    {
        return $this->hasMany(
            'App\Model\MatchesHasConcurrentTeam',
            'concurrent_team_id'
        );
    }
}
