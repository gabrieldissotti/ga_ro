<?php

namespace App\Model;

class MatchesHasConcurrentTeam extends AppModel
{
    public function match()
    {
        return $this->belongsTo('App\Model\Match', 'match_id');
    }

    public function concurrent_team()
    {
        return $this->belongsTo('App\Model\ConcurrentTeam', 'concurrent_team_id');
    }
}
