<?php

namespace App\Model;

class Group extends AppModel
{
    public function concurrent_teams()
    {
        return $this->hasMany('App\Model\ConcurrentTeam', 'group_id');
    }

    public function events()
    {
        return $this->belongsTo('App\Model\Event', 'event_id');
    }
}
