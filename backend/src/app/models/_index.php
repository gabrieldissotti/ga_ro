<?php

require __DIR__.'/AppModel.php';
require __DIR__.'/Event.php';
require __DIR__.'/Team.php';
require __DIR__.'/Group.php';
require __DIR__.'/ConcurrentTeam.php';
require __DIR__.'/Match.php';
require __DIR__.'/MatchesHasConcurrentTeam.php';
require __DIR__.'/ApiToken.php';
