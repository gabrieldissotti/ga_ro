<?php

namespace App\Model;

class Match extends AppModel
{
    public function matches_has_concurrent_teams()
    {
        return $this->hasMany('App\Model\MatchesHasConcurrentTeam', 'match_id');
    }
}
