<?php

namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Model;

class Events
{
    /**
     * @OA\Get(
     *     path="/api/events",
     *     summary="List all events",
     *     tags={"Events"},
     *     operationId="findEvents",
     *     @OA\Response(
     *       response=400,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Token no provided",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Inválid token",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "id": 1,
     *                 "title": "A"
     *               },
     *               {
     *                 "id": 2,
     *                 "title": "B"
     *               },
     *           }
     *       )
     *     )
     * )
     */
    public function index(Request $req, Response $res, $args)
    {
        try {
            $events = Model\Event::get();

            return $res->withJson($events);
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }

    /**
     * @OA\Get(
     *     path="/api/events/{event_id}",
     *     summary="show all event data",
     *     tags={"Events"},
     *     operationId="findEvent",
     *     @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "id": 1,
     *                 "title": "A",
     *                 "start_date": "2019-11-10 00:00:00"
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=400,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Token no provided",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Inválid token",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Parameter(
     *         name="event_id",
     *         in="path",
     *         description="Event id",
     *         required=true,
     *         style="form",
     *     )
     * )
     */
    public function show(Request $req, Response $res, $args)
    {
        try {
            $event = Model\Event::where('id', $args['id'])->get();

            return $res->withJson($event);
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }
}
