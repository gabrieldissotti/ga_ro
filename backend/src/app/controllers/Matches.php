<?php

namespace App\Controller;

use App\Model;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Matches
{
    /**
     * @OA\Get(
     *     path="/api/matches",
     *     summary="List all matches",
     *     tags={"Matches"},
     *     operationId="findMatches",
     *     @OA\Parameter(
     *         name="stage",
     *         in="query",
     *         description="When the stage is playoffs, then the group_id is required*",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             enum={"groups", "playoffs"},
     *             default="groups"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="group_id",
     *         in="query",
     *         description="filter matches by group",
     *         required=false,
     *         style="form",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             example={
     *                 {
     *                   "match_id": 161,
     *                   "level": 1,
     *                   "team_title": "Koelpin and Sons",
     *                   "team_logo_url": "https:\/\/picsum.photos\/id\/71\/25\/25",
     *                   "rounds": 16,
     *                   "winner": 1
     *                 },
     *                 {
     *                   "match_id": 161,
     *                   "level": 1,
     *                   "team_title": "Parisian-Maggio",
     *                   "team_logo_url": "https:\/\/picsum.photos\/id\/42\/25\/25",
     *                   "rounds": 10,
     *                   "winner": 0
     *                 }
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *       response=400,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Token no provided",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Inválid token",
     *               }
     *           }
     *       ),
     *     ),
     * )
     */
    public function index(Request $req, Response $res, $args)
    {
        try {
            $params = $req->getQueryParams();

            $matches = Model\Match::join(
                'matches_has_concurrent_teams',
                'matches_has_concurrent_teams.match_id',
                'matches.id'
            )->join(
                'concurrent_teams',
                'concurrent_teams.id',
                'matches_has_concurrent_teams.concurrent_team_id'
            )->join(
                'teams',
                'teams.id',
                'concurrent_teams.team_id'
            )->select([
                'matches.id as match_id',
                'matches.level as level',
                'teams.id as team_id',
                'teams.title as team_title',
                'teams.logo_url as team_logo_url',
                'matches_has_concurrent_teams.rounds as rounds',
                'matches_has_concurrent_teams.winner as winner',
            ]);

            // Filters by stage param
            if (isset($params['stage'])) {
                $matches->where('stage', $params['stage']);
            }

            // Filters by group_id param
            if (isset($params['group_id'])) {
                $matches->where('group_id', $params['group_id']);
            }

            return $res->withJson($matches->get());
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }
}
