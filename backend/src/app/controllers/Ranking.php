<?php

namespace App\Controller;

use App\Model;
use Illuminate\Database\Capsule\Manager as Capsule;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class Ranking
{
    /**
     * @OA\Get(
     *     path="/api/ranking",
     *     summary="List the ranking of teams",
     *     tags={"Ranking"},
     *     operationId="findRanking",
     *     @OA\Parameter(
     *         name="group_id",
     *         in="query",
     *         description="filter ranking by group",
     *         required=false,
     *         style="form",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *             example={
     *                 {
     *                   "team_id": 51,
     *                   "total_rounds": "64",
     *                   "total_wins": "4",
     *                   "classification": 1,
     *                   "team": {
     *                     "id": 51,
     *                     "title": "Cole, Jacobs and Renner",
     *                     "logo_url": "https://picsum.photos/100/100"
     *                   }
     *                 },
     *                 {
     *                   "team_id": 31,
     *                   "total_rounds": "64",
     *                   "total_wins": "4",
     *                   "classification": 2,
     *                   "team": {
     *                     "id": 31,
     *                     "title": "Harber-Kovacek",
     *                     "logo_url": "https://picsum.photos/100/100"
     *                   }
     *                 },
     *             }
     *         )
     *     ),
     *     @OA\Response(
     *       response=400,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Token no provided",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Inválid token",
     *               }
     *           }
     *       ),
     *     ),
     * )
     */
    public function index(Request $req, Response $res, $args)
    {
        try {
            $params = $req->getQueryParams();

            $concurrent_teams = Model\ConcurrentTeam::with('team')
            ->join(
                'matches_has_concurrent_teams',
                'matches_has_concurrent_teams.concurrent_team_id',
                'concurrent_teams.id'
            )
            ->groupBy('concurrent_teams.id')
            ->join(
                'matches',
                'matches_has_concurrent_teams.match_id',
                'matches.id'
            )
            ->where('stage', 'groups')
            ->orderBy('total_wins', 'DESC')
            ->orderBy('total_rounds', 'DESC');

            // Filters by group_id param
            if (isset($params['group_id'])) {
                $concurrent_teams
            ->join(
                'groups',
                'concurrent_teams.group_id',
                'groups.id'
            )
            ->where('group_id', $params['group_id']);
            }

            $concurrent_teams->select(
            'concurrent_teams.team_id',
            Capsule::raw('
                sum( rounds ) as total_rounds,
                sum( winner ) as total_wins
            ')
        );

            $ranking = $concurrent_teams->get();

            foreach ($ranking as $key => $value) {
                $ranking[$key]['classification'] = $key + 1;
            }

            return $res->withJson($ranking);
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }
}
