<?php

namespace App\Controller;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Model;

class EventGroups
{
    /**
     * @OA\Get(
     *     path="/api/events/{event_id}/groups",
     *     summary="List all groups by event",
     *     tags={"Groups", "Events"},
     *     operationId="findEventGroups",
     *     @OA\Response(
     *       response=200,
     *       description="successful operation",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "id": 1,
     *                 "title": "A"
     *               },
     *               {
     *                 "id": 2,
     *                 "title": "B"
     *               },
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=400,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Token no provided",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Response(
     *       response=401,
     *       description="bad request",
     *       @OA\JsonContent(
     *           example={
     *               {
     *                 "message": "Inválid token",
     *               }
     *           }
     *       ),
     *     ),
     *     @OA\Parameter(
     *         name="event_id",
     *         in="path",
     *         description="Event id to filter groups",
     *         required=true,
     *         style="simple",
     *     )
     * )
     */
    public function index(Request $req, Response $res, $args)
    {
        try {
            $groups = Model\Event::where('events.id', $args['id'])
                ->join(
                    'groups',
                    'groups.event_id',
                    'events.id'
                )
                ->orderBy('title', 'ASC')
                ->select(['groups.id', 'groups.title'])
                ->get();

            return $res->withJson($groups);
        } catch (\Exception $err) {
            return $res
              ->withStatus(500)
              ->withJson(['message' => $err->getMessage()]);
        }
    }
}
