<?php

use Phinx\Migration\AbstractMigration;

class CreateMatches extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('matches');

        $table->addColumn('stage', 'string')
            ->addColumn('level', 'integer')
            ->create();
    }

    public function down()
    {
        $this->table('matches')->drop()->save();
    }
}
