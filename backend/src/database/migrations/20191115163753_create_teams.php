<?php

use Phinx\Migration\AbstractMigration;

class CreateTeams extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('teams');
        $table->addColumn('title', 'string', ['limit' => 255])
              ->addColumn('logo_url', 'string', ['limit' => 255])
              ->create();
    }

    public function down()
    {
        $this->table('teams')->drop()->save();
    }
}
