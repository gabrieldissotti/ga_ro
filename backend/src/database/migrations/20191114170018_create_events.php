<?php

use Phinx\Migration\AbstractMigration;

class CreateEvents extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('events');
        $table->addColumn('title', 'string', ['limit' => 255])
            ->addColumn('start_date', 'timestamp')
            ->create();
    }

    public function down()
    {
        $this->table('events')->drop()->save();
    }
}
