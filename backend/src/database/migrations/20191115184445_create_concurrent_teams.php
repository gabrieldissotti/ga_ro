<?php

use Phinx\Migration\AbstractMigration;

class CreateConcurrentTeams extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('concurrent_teams');

        $table->addColumn('group_id', 'integer')
            ->addColumn('team_id', 'integer')
            ->addForeignKey('group_id', 'groups', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
            ])
            ->addForeignKey('team_id', 'teams', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
            ])
            ->create();
    }

    public function down()
    {
        $this->table('concurrent_teams')->drop()->save();
    }
}
