<?php

use Phinx\Migration\AbstractMigration;

class CreateApiTokens extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('api_tokens');

        $table->addColumn('email', 'string')
            ->addColumn('encryption_key', 'string')
            ->addColumn('api_key', 'string')
            ->create();
    }

    public function down()
    {
        $this->table('api_tokens')->drop()->save();
    }
}
