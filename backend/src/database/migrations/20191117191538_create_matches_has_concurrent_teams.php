<?php

use Phinx\Migration\AbstractMigration;

class CreateMatchesHasConcurrentTeams extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('matches_has_concurrent_teams');

        $table->addColumn('match_id', 'integer')
            ->addColumn('concurrent_team_id', 'integer')
            ->addColumn('rounds', 'integer')
            ->addColumn('winner', 'boolean')
            ->addForeignKey('match_id', 'matches', 'id', [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ])
            ->addForeignKey('concurrent_team_id', 'concurrent_teams', 'id', [
                'delete' => 'CASCADE',
                'update' => 'CASCADE',
            ])
            ->create();
    }

    public function down()
    {
        $this->table('matches_has_concurrent_teams')->drop()->save();
    }
}
