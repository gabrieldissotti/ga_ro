<?php

use Phinx\Migration\AbstractMigration;

class CreateGroups extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('groups');

        $table->addColumn('title', 'string', ['limit' => 255])
            ->addColumn('event_id', 'integer')
            ->addForeignKey('event_id', 'events', 'id', [
                'delete' => 'NO_ACTION',
                'update' => 'NO_ACTION',
            ])
            ->create();
    }

    public function down()
    {
        $this->table('groups')->drop()->save();
    }
}
