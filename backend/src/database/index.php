<?php

namespace App;

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

require_once __DIR__.'/../lib/Dotenv.php';

class Database
{
    public function __construct()
    {
        $this->capsule = new Capsule();

        $this->handleMysqlConnection();

        Capsule::enableQueryLog();
    }

    public function handleMysqlConnection()
    {
        $this->capsule->addConnection([
            'driver' => 'mysql',
            'host' => getenv('MYSQL_HOST'),
            'port' => getenv('MYSQL_PORT'),
            'database' => getenv('MYSQL_NAME'),
            'username' => getenv('MYSQL_USER'),
            'password' => getenv('MYSQL_PASS'),
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ]);

        $this->capsule->setEventDispatcher(new Dispatcher(new Container()));

        $this->capsule->setAsGlobal();

        $this->capsule->bootEloquent();
    }
}
