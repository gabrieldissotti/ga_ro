<?php

use Phinx\Seed\AbstractSeed;

class ApiTokens extends AbstractSeed
{
    public function run()
    {
        $table = $this->table('api_tokens');

        $data = [
            [
                'email' => 'gabrieldnrodrigues@gmail.com',
                'encryption_key' => 'hdas0h83h40hwra3dp8rp8wyrywng2w3434',
                'api_key' => 'aay74ytelrhh375yq4ihq43iht754owdhn3ohfto75folghofo',
            ],
            [
                'email' => 'gabriel.rodrigues31@fatec.sp.gov.br',
                'encryption_key' => 'rjq824tnqd8y4pc84c8pqcy43tc4ct343',
                'api_key' => '4q27lyftl75qhl3ndt8h3GWTEGF4gro4qn4uhilntgheunhg',
            ],
        ];

        $table->insert($data)->save();
    }
}
