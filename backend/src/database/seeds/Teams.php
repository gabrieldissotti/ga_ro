<?php

use Phinx\Seed\AbstractSeed;

class Teams extends AbstractSeed
{
    public function run()
    {
        $table = $this->table('teams');

        $faker = Faker\Factory::create();
        for ($i = 0; $i < 80; ++$i) {
            $photoId = $i + 1;
            $data[] = [
                'title' => $faker->company,
                'logo_url' => "https://picsum.photos/id/{$photoId}/25/25",
            ];
        }

        $table->insert($data)
              ->save();
    }
}
