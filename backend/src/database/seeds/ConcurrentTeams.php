<?php

use Phinx\Seed\AbstractSeed;

class ConcurrentTeams extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'Groups',
            'Teams',
        ];
    }

    public function run()
    {
        $table = $this->table('concurrent_teams');

        $data = [];
        for ($groupId = 1; $groupId <= 16; ++$groupId) {
            for ($teamId = ($groupId * 5 - 4); $teamId <= $groupId * 5; ++$teamId) {
                $data[] = [
                    'group_id' => $groupId,
                    'team_id' => $teamId,
                ];
            }
        }

        $table->insert($data)->save();
    }
}
