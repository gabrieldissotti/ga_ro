<?php

use Phinx\Seed\AbstractSeed;

class Events extends AbstractSeed
{
    public function run()
    {
        $table = $this->table('events');

        $table->insert([
            'title' => 'GC Test Fake Event IV',
            'start_date' => '2019-11-10 00:00:00',
        ])->save();
    }
}
