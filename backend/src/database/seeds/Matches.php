<?php

use Phinx\Seed\AbstractSeed;

class Matches extends AbstractSeed
{
    public function run()
    {
        $matches = $this->getGroupStageMatches();

        $lastMatchId = count($matches['matches']);

        $this->table('matches')
            ->insert($matches['matches'])
            ->save();

        $this->table('matches_has_concurrent_teams')
            ->insert($matches['matches_has_concurrent_teams'])
            ->save();

        $classifiedToPlayoffs = $this->getClassifiedTeamsIds($matches['matches_has_concurrent_teams']);

        $playoffMatches = $this->getPlayoffsMatches($classifiedToPlayoffs, $lastMatchId);

        $this->table('matches')
            ->insert($playoffMatches['matches'])
            ->save();

        $this->table('matches_has_concurrent_teams')
            ->insert($playoffMatches['matches_has_concurrent_teams'])
            ->save();
    }

    public function generateGroupsAndTeams()
    {
        $data = [];
        for ($groupId = 1; $groupId <= 16; ++$groupId) {
            for ($teamId = ($groupId * 5 - 4); $teamId <= $groupId * 5; ++$teamId) {
                $data[$groupId][] = [
                    'team_id' => $teamId,
                ];
            }
        }

        return $data;
    }

    public function getGroupStageMatches()
    {
        $matches = [];

        $groups = $this->generateGroupsAndTeams();

        $match_id = 1;

        foreach ($groups as $groupId => $teams) {
            foreach ($teams as $teamAKey => $teamA) {
                foreach ($teams as $teamBKey => $teamB) {
                    if ($teamA['team_id'] === $teamB['team_id']) {
                        continue;
                    }

                    $matches['matches'][] = [
                        'id' => $match_id,
                        'stage' => 'groups',
                        'level' => 1,
                    ];

                    $matches['matches_has_concurrent_teams'][] = [
                        'match_id' => $match_id,
                        'concurrent_team_id' => $teamA['team_id'],
                        'rounds' => 16,
                        'winner' => true,
                    ];

                    $matches['matches_has_concurrent_teams'][] = [
                        'match_id' => $match_id,
                        'concurrent_team_id' => $teamB['team_id'],
                        'rounds' => rand(0, 15),
                    ];

                    ++$match_id;
                }

                unset($teams[$teamAKey]);
            }
        }

        return $matches;
    }

    public function getClassifiedTeamsIds($matches)
    {
        $teams = $this->addPointsAndTotalRoundsToMatches($matches);

        $teamsIds = $this->filterJustClassifiedTeamsIds($teams);

        return $teamsIds;
    }

    public function filterJustClassifiedTeamsIds($teams)
    {
        $classifiedTeamsIds = [];

        foreach ($teams as $key => $team) {
            if ($team['wins'] >= 3) {
                $classifiedTeamsIds[] = $team['concurrent_team_id'];
                unset($teams[$key]);
            }
        }

        return $classifiedTeamsIds;
    }

    public function addPointsAndTotalRoundsToMatches($matches)
    {
        $groupedTeams = [];
        foreach ($matches as $key => $match) {
            if (!isset($groupedTeams[$match['concurrent_team_id']])) {
                $groupedTeams[$match['concurrent_team_id']] = $match;
                $groupedTeams[$match['concurrent_team_id']]['wins'] = 0;
            }

            $groupedTeams[$match['concurrent_team_id']]['rounds'] += $match['rounds'];

            if (!empty($match['winner'])) {
                ++$groupedTeams[$match['concurrent_team_id']]['wins'];
            }
        }

        return $groupedTeams;
    }

    public function groupRandomTeams($teams)
    {
        shuffle($teams);

        $groups = array_chunk($teams, 2);

        return $groups;
    }

    public function getPlayoffsMatches($teams, $lastMatchId)
    {
        $matches = [];
        $level = 1;

        $match_id = $lastMatchId + 1;

        while (count($teams) > 1) { // create recursive matches
            $groups = $this->groupRandomTeams($teams);

            foreach ($groups as $key => $team) { // create level matches
                $matches['matches'][] = [
                    'id' => $match_id,
                    'stage' => 'playoffs',
                    'level' => $level,
                ];

                $matches['matches_has_concurrent_teams'][] = [
                    'match_id' => $match_id,
                    'concurrent_team_id' => $team[0],
                    'rounds' => 16,
                    'winner' => true,
                ];

                $matches['matches_has_concurrent_teams'][] = [
                    'match_id' => $match_id,
                    'concurrent_team_id' => $team[1],
                    'rounds' => rand(0, 15),
                ];

                ++$match_id;
            }
            ++$level;

            $teams = [];

            foreach ($groups as $key => $team) {
                $teams[] = $team[0];
            }
        }

        return $matches;
    }
}
