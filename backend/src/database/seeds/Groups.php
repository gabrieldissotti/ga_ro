<?php

use Phinx\Seed\AbstractSeed;

class Groups extends AbstractSeed
{
    public function getDependencies()
    {
        return [
            'Events',
        ];
    }

    public function run()
    {
        $table = $this->table('groups');

        $data = [
            ['title' => 'A', 'event_id' => 1],
            ['title' => 'B', 'event_id' => 1],
            ['title' => 'C', 'event_id' => 1],
            ['title' => 'D', 'event_id' => 1],
            ['title' => 'E', 'event_id' => 1],
            ['title' => 'F', 'event_id' => 1],
            ['title' => 'G', 'event_id' => 1],
            ['title' => 'H', 'event_id' => 1],
            ['title' => 'I', 'event_id' => 1],
            ['title' => 'J', 'event_id' => 1],
            ['title' => 'K', 'event_id' => 1],
            ['title' => 'L', 'event_id' => 1],
            ['title' => 'M', 'event_id' => 1],
            ['title' => 'N', 'event_id' => 1],
            ['title' => 'O', 'event_id' => 1],
            ['title' => 'P', 'event_id' => 1],
        ];

        $table->insert($data)->save();
    }
}
