<?php

header('Access-Control-Allow-Origin: http://localhost:3000');
header('Access-Control-Allow-Headers: Authorization');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');
header('Access-Control-Expose-Headers: DAV, content-length, Allow');
header('Access-Control-Allow-Methods: GET, OPTIONS, POST, PUT');

require 'src/server.php';
