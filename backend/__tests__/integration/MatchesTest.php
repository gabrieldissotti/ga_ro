<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class MatchesTest extends TestCase
{
    public function testMatchesShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/matches');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testWhenAllParamsIsSentThenTheMatchesShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/matches', [
            'query' => [
                'stage' => 'groups',
                'group_id' => 1,
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
