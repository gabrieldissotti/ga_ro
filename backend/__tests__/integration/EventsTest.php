<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class EventsTest extends TestCase
{
    public function testEventsShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/events');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testEventsShouldBeShowed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/events/1');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
