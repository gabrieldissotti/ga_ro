<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class EventGroupsTest extends TestCase
{
    public function testGroupsShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/events/1/groups');

        $this->assertEquals(200, $response->getStatusCode());
    }
}
