<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;

final class RankingTest extends TestCase
{
    public function testRankingShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/ranking');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testWhenAllParamsIsSentThenTheRankingShouldBeListed(): void
    {
        $client = new Client([
            'base_uri' => 'http://127.0.0.1:3344',
        ]);

        $response = $client->get('/ranking', [
            'query' => [
                'group_id' => 1,
            ],
        ]);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
