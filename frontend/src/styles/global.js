import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Oswald|Poppins&display=swap');

  * {
    margin: 0;
    padding: 0;
    outline: 0;
    box-sizing: border-box;
  }

  html {
    min-height: 100%;
  }

  body {
    min-height: 100%;

    background: linear-gradient(360deg, #323331 0%, #2C2C2C 100%);
    -webkit-font-smoothing: antialiased;
  }

  body, input, button {
    font: 14px Poppins, sans-serif;
  }

  #root {
    margin: 0 auto;
    padding: 0;
    min-height: 100%;
  }

  button {
    cursor: pointer;
  }
`;
