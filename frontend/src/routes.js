import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './pages/Home';
import Playoffs from './pages/Playoffs';

export default function Routes() {
  return (
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/playoffs" exact component={Playoffs} />
    </Switch>
  );
}
