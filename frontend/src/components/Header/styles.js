import styled, { css } from 'styled-components';

export const Container = styled.header`
  width: 100%;
  height: 59px;
  background-color: #282c43;

  display: flex;
  justify-content: center;
  align-items: center;

  border-radius: 0 0 25px 0;

  @media only screen and (max-width: 600px) {
    flex-wrap: wrap;
    height: 100px;
  }
`;

export const Nav = styled.nav`
  max-width: 426px;
  width: 100%;

  display: flex;
  justify-content: space-around;
`;

export const Button = styled.div`
  margin: 2px;

  width: 189px;
  height: 41px;

  background: linear-gradient(
    90deg,
    rgba(74, 69, 169, 0.4) 0%,
    rgba(43, 186, 227, 0.4) 100%
  );
  border-radius: 26px;
  font-family: Oswald;
  line-height: 27px;
  font-weight: normal;
  font-style: normal;
  font-size: 18px;
  color: #ffffff;

  display: flex;
  justify-content: space-around;
  align-items: center;

  cursor: pointer;

  ${props =>
    props.active &&
    css`
      background: linear-gradient(90deg, #4a45a9 0%, #2bbae3 100%);
    `}

  @media only screen and (max-width: 600px) {
    width: 160px;
  }
`;

export const Title = styled.a`
  position: absolute;
  right: 38px;

  font-family: Oswald;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 27px;
  color: #ffffff;

  @media only screen and (max-width: 917px) {
    right: auto;
    position: relative;
  }
`;
