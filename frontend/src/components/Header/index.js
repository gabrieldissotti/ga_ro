import React from 'react';

import { Container, Button, Nav, Title } from './styles';
import { Link, withRouter } from 'react-router-dom';

export default withRouter(function Header({ location }) {
  return (
    <Container>
      <Nav>
        <Link to="/">
          <Button active={ location.pathname === '/'}>fase de groups</Button>
        </Link>
        <Link to="/playoffs">
          <Button active={ location.pathname === '/playoffs'}>fase de playoffs</Button>
        </Link>
      </Nav>
      <Title>DreamHack Atlanta Fake 2019</Title>
    </Container>
  );
})
