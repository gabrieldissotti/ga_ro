import React, { useState, useEffect } from 'react';

import { Container, Icon } from './styles';

export default function TableMatches({ data }) {
  const [width, setWidth] = useState(window.innerWidth);

  function updateWindowDimensions() {
    setWidth(window.innerWidth);
  }

  useEffect(() => {
    updateWindowDimensions();
    window.addEventListener('resize', updateWindowDimensions);
  }, []);

  return (
    <Container>
      <thead>
        <tr>
          <th>Equipe</th>
          <th>Rounds</th>
          <th>Adversário</th>
        </tr>
      </thead>
      <tbody>
      { data.length > 0 &&
        data.map((item, index) =>
          <tr key={index}>
            <td>
              <div>
                <p>{item[0].team_title}</p>
                {width > 600 && <img src={item[0].team_logo_url} alt={item[0].team_title} />}
              </div>
            </td>
            <td>
              {item[0].rounds} x {item[1].rounds}
            </td>
            <td>
              <div>
                {width > 600 && <img src={item[1].team_logo_url} alt={item[1].team_title} />}
                <p>{item[1].team_title}</p>
              </div>
            </td>
          </tr>
        )}
      </tbody>
    </Container>
  );
}
