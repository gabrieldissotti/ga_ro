import styled from 'styled-components';

export const Container = styled.table`
  width: 100%;

  border: 1px solid #ccc;
  border-collapse: collapse;
  text-align: left;

  td,
  th {
    padding: 3px 5px;

    border: none;
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 18px;
    color: #ffffff;

    div {
      display: flex;
      align-items: center;
      img {
        margin-right: 4px;
      }
    }
  }

  &,
  th,
  td {
    border: none;
  }

  thead {
    tr {
      background: #3692cf;

      th {
      }
    }
  }

  tbody {
    tr {
      td {
      }

      &:nth-child(even) {
        background: rgba(255, 255, 255, 0.04);
      }
    }
  }
`;

export const Icon = styled.div``;
