import React, { useState, useEffect } from 'react';

import { Container, Icon } from './styles';

export default function TableRanking({ data }) {
  const [width, setWidth] = useState(window.innerWidth);

  function updateWindowDimensions() {
    setWidth(window.innerWidth);
  }

  useEffect(() => {
    updateWindowDimensions();
    window.addEventListener('resize', updateWindowDimensions);
  }, []);

  return (
    <Container>
      <thead>
        <tr>
          <th>Class.</th>
          <th>Equipe</th>
          <th>Pts.</th>
          <th>Rounds</th>
        </tr>
      </thead>
      <tbody>
        { data &&
          data.map((item) => <tr key={item.team.id}>
          <td>
            <Icon />
            {item.classification}º
          </td>
          <td>
            <div>
              {width > 600 && <img src={item.team.logo_url} alt={item.team.title} />}
              <p>{item.team.title}</p>
            </div>
          </td>
          <td>{item.total_wins}</td>
          <td>{item.total_rounds}</td>
        </tr>)
        }
      </tbody>
    </Container>
  );
}
