import styled from 'styled-components';

export const Container = styled.div`
  width: max-content;

  font-family: Poppins;
  font-weight: normal;
  line-height: 27px;
  font-style: normal;
  font-size: 15px;
  color: #F14668;

  margin: 30px;

  /*
  *  Flex Layout Specifics
  */
  main{
    display:flex;
    flex-direction:row;
  }
  .round{
    display:flex;
    flex-direction:column;
    justify-content:center;
    width:200px;
    list-style:none;
    padding:0;
  }
  .round .spacer{ flex-grow:1; }
  .round .spacer:first-child,
  .round .spacer:last-child{ flex-grow:.5; }

  .round .game-spacer{
    flex-grow:1;
  }

  /*
  *  General Styles
  */
  body{
    font-family:sans-serif;
    font-size:small;
    padding:10px;
    line-height:1.4em;
  }

  li.game{
    position: relative;
    padding-left:20px;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  li.game.winner{
    font-weight:bold;
    color: #48C774;
  }
  li.game span{
    position: absolute;
    right: 0;
    top: -5px;
    font-size: 10px;
  }

  li.game-top{ border-bottom:1px solid #6554ff;}
  li.game-bottom{
    border-top:1px solid #6554ff;
  }

  li.game-spacer{
    border-right:1px solid #6554ff;
    min-height:40px;
  }

`;
