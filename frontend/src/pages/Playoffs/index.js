import React, { useState, useEffect, Fragment }  from 'react';

import { Container } from './styles';
import api from '../../services/api';

export default function Playoffs() {
  const [matches, setMatches] = useState([]);
  const [lastWinner, setLastWinner] = useState([]);

  async function groupByMatches(matches) {
    let m = [];

    await matches.data.forEach((match) => {
      if(m[match.match_id] === undefined){
        m[match.match_id] = [];
      }

      m[match.match_id].push(match);
    })

    return m;
  }

  async function groupByLevels(matches) {
    let m = [];

    await matches.forEach((match) => {
      if(m[match[0].level] === undefined){
        m[match[0].level] = [];
      }

      m[match[0].level].push(match);
    })

    return m;
  }

  async function handleMatches() {
    const matches = await api.get('/matches', {
      params: {
        stage: 'playoffs'
      }
    });

    if(matches){
      let formatedMatches = await groupByMatches(matches);

      formatedMatches = await groupByLevels(formatedMatches);

      setMatches(formatedMatches);

      setLastWinner(formatedMatches[(formatedMatches.length-1)][0][0])
    }
  }

  useEffect(() => {
    handleMatches()
  }, []);

  const getLis = (level) => {
    return level.map((match, match_index) =>
      <Fragment key={match_index}>
        <li className={`game game-top ${!!match[0].winner && 'winner'}`}>{match[0].team_title} <span>{match[0].rounds}</span></li>
        <li className="game game-spacer">&nbsp;</li>
        <li className={`game game-bottom ${!!match[1].winner && 'winner'}`}>{match[1].team_title}<span>{match[1].rounds}</span></li>

        <li className="spacer">&nbsp;</li>
      </Fragment>
    )
  }

  return (
    <Container>
      <main id="tournament">
        {
          matches && matches.map((levelMatches, level) =>
            <ul className={`round round-${level}`} key={level}>
              <li className="spacer">&nbsp;</li>
              { getLis(levelMatches) }
            </ul>
          )
        }

        <ul className={`round round-${matches.length+1}`} >
          <li className="spacer">&nbsp;</li>
          <li className="game game-top  winner">{lastWinner.team_title}<span>{lastWinner.rounds}</span></li>
          <li className="spacer">&nbsp;</li>
        </ul>
      </main>
    </Container>
    );
  }
