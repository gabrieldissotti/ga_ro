import styled, { css } from 'styled-components';
import arrowImage from '../../assets/images/arrow.png';

export const Container = styled.div`
  margin: auto 36px;
`;

export const Filters = styled.div`
  display: flex;
`;

export const Legend = styled.p`
  padding-right: 13px;

  font-family: Oswald;
  font-weight: normal;
  line-height: 27px;
  font-style: normal;
  font-size: 18px;
  color: #b0b0b0;
`;

export const Groups = styled.select`
  width: 174px;
  height: 31px;

  padding: 6px 17px;

  color: #ffffff;
  border: none;
  font-size: 12px;
  background: ${`url(${arrowImage}) no-repeat right`} #4f504e;
  font-family: Poppins, sans-serif;
  line-height: 18px;
  border-radius: 0px 5px 0px 12px;

  appearance: none;
  -moz-appearance: none;
  -webkit-appearance: none;
`;

export const Wrapper = styled.div`
  position: relative;
  margin-top: 44px;
  width: 100%;

  display: grid;
  grid-template-columns: 1fr 2fr;

  @media only screen and (max-width: 917px) {
    display: flex;
    flex-wrap: wrap;

    justify-content: center;
    align-items: center;
  }
`;

export const Section = styled.div`
  h3 {
    position: relative;
    top: -13px;
    left: 10px;

    font-family: Oswald;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 27px;
    color: #ffffff;
  }

  background: #363735;
  border-radius: 5px;

  min-height: 297px;

  @media only screen and (max-width: 917px) {
    width: 100%;
    margin-bottom: 20px;

    min-height: 170px;

  }

  @media only screen and (min-width: 917px) {
    &:first-of-type {
      margin-right: 8px;
    }

    &:last-of-type {
      margin-left: 8px;
    }
  }
`;
