import React, { useState, useEffect } from 'react';

import { Container, Filters, Legend, Groups, Wrapper, Section } from './styles';

import TableRanking from '../../components/TableRanking';
import TableMatches from '../../components/TableMatches';
import api from '../../services/api';

export default function Home() {
  const [width, setWidth] = useState(window.innerWidth);
  const [groups, setGroups] = useState([]);
  const [matches, setMatches] = useState([]);
  const [ranking, setRanking] = useState([]);

  function updateWindowDimensions() {
    setWidth(window.innerWidth);
  }

  async function handleGroups() {
    const groups = await api.get('/events/1/groups');

    if(groups){
      setGroups(groups.data);
    }
  }

  async function handleRanking(groupId = false) {
    const ranking = await api.get('/ranking', {
      params: {
        ...(!!groupId && { group_id: groupId })
      }
    });

    if(ranking){
      setRanking(ranking.data);
    }
  }

  async function handleMatches(groupId = false) {
    const matches = await api.get('/matches', {
      params: {
        stage: 'groups',
        ...(!!groupId && { group_id: groupId })
      }
    });

    if(matches){
      let m = [];
      await matches.data.forEach((match) => {
        if(m[match.match_id] === undefined){
          m[match.match_id] = [];
        }

        m[match.match_id].push(match);
      })

      setMatches(m);
    }
  }

  useEffect(() => {
    updateWindowDimensions();
    window.addEventListener('resize', updateWindowDimensions);

    handleGroups()
    handleRanking()
    handleMatches()
  }, []);

  function handleChangeFilter(e){
    const groupId = e.target.value;
    handleRanking(groupId)
    handleMatches(groupId)
  }

  return (
    <Container>
      <Filters>
        <Legend>Filtros:</Legend>
        <Groups onChange={e => handleChangeFilter(e)}>
          <option value="">todos os grupos</option>
          { groups &&
            groups.map((group) => <option value={group.id} key={group.id}>Grupo {group.title}</option>)
          }
        </Groups>
      </Filters>
      <Wrapper>
        <Section>
          <h3>Ranking</h3>

          <TableRanking data={ranking} />
        </Section>
        <Section>
          <h3>Resultados</h3>
          <TableMatches  data={matches} />
        </Section>
      </Wrapper>
    </Container>
  );
}
