import axios from 'axios';

const api = axios.create({
  baseURL: 'http://127.0.0.1:3344/api',
  headers: {
    'Authorization': 'Bearer hdas0h83h40hwra3dp8rp8wyrywng2w3434'
  }
});

export default api;
