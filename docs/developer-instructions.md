### Developer instructions

Check some important instructions about development with this project

1. Create commits with the [Commitizen](https://github.com/commitizen/cz-cli) structure.

2. You need use the [GitFlow](https://danielkummer.github.io/git-flow-cheatsheet/) workflow.

3. If your update a MySQL model or Mongoose schema, please migrate the database data and updated the model in docs