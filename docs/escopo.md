# Escopo


Sabendo que somos uma plataforma que tem como um de seus principais atrativos a competição e disputa entre equipes, resolvemos criar um campeonato. Para isso precisamos fazer uma simulação e é aí que você vai nos ajudar. O campeonato acontecerá em duas fases, que serão:


1. Fase de grupos:

○ Esta fase conta com 80 equipes, divididas em grupos de 5 times.

○ Os times jogam uma vez contra todos os outros que estiverem dentro do seu grupo.

○ A partida se encerra quando um dos times ganha 16 "rounds".

○ O vencedor soma 1 ponto na tabela.

○ Apenas os 2 times com mais pontos de cada grupo se classificam para a próxima fase de Playoffs

○ Os critérios de desempate são o número de vitórias, e saldo de "rounds" a favor.

> Resultado: Sabendo dessas condições, apresente os grupos com os respectivos times, os jogos com seus respectivos resultados, e ordene os times por maior número de vitórias.

2. Playoffs:

○ Os times da fase anterior formarão duplas aleatoriamente para se enfrentar.

○ Os perdedores serão diretamente eliminados.

○ Os ganhadores chegarão até a final, no formato "Bracket" - Eliminação simples.

> Resultado: Com essas informações definidas, apresente os jogos com seus respectivos resultados até conhecermos o grande campeão.

Levando em consideração as especificações acima, esperamos que a solução seja desenvolvida utilizando uma arquitetura de APIs privadas:

Tecnologias e outras especificações a serem utilizadas.

Você deve realizar o teste com as seguintes tecnologias:



● Front: A seu critério. Foque nas funcionalidades, não na aparência!

● Back: golang java php ruby python.

● Database: A seu critério.


