# GC Challenge 2019 Backend Pleno

Desafio para desenvolvedor back-end pleno.


### Tecnologias utilizadas:

- PHP 7.2 (Backend)
- ReactJS (Frontend)
- MySql8 (Database)

#### Resumo do que foi utilizado:

- Padrão APIs REST (Privadas com API token)
- Swagger (Documentação) 
- PHPUnit (TDD)
- Migrations (phinx)
- Seeds (phinx)
- Docker
- AWS EC2 (servidor)
- HTTPS com letsencrypt (para testes)


### Veja o projeto funcionando:

### [ir para o site](https://gc.dissotti.com)

### outros links:

[ver desenho da arquitetura](./docs/arquitetura.png)

[ver modelagem](./docs/Model.png)

[ver documentação da API](https://api.gc.dissotti.com/docs)

[ver o escopo do projeto](./docs/escopo.md)

[ver instruções do workflow para desenvolvedores](./docs/developer-inscructions.md)

---


## Como rodar

### No ambiente de desenvolvimento

1. Certifique-se de ter instalado as seguintes dependências:

> A aplicação foi testada nas versões dos pacotes/softwares específicadas abaixo.

- Ubuntu (v19.10)
- PHP (7.2)
- Docker (v19.03.3, build a872fc2f86)
- Docker Compose (v1.24.1, build 4667896b)
- Composer (1.8.6)
- Node (10.16.3)
- Yarn (1.19.1)

>  instale as extensões do php "php7.2-xml", "php7.2-mysql" e "php7.2-mbstring" para utilizar o "composer start" e subir a aplicação sem problemas. Se não for utilizar o script em docker, verifique no arquivo docker-compose.yml as versões do Apache e do MySQL.

2. De permissão para todos os arquivos do projeto:

```sh
# linux
chmod -R 777 ./
```

3. Inicie o servidor (backend):

> Se você setiver rodando um servidor Mysql na porta 3306, você deve pará-lo (service mysql stop)

```sh
# dentro de "./backend" (cd backend)
composer start 
```

4. Inicie o cliente (frontend):

```sh
# dentro de "./frontend" (cd frontend)
yarn && yarn start 
```

Tudo pronto! veja algumas observações:

> Todas as rotas requerem um token, para ambiente de desenvolvimento pode ser usado o token hdas0h83h40hwra3dp8rp8wyrywng2w3434 presente nos seeds
